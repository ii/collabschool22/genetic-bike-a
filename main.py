# this is the main program, which you should run with python .main.py in your terminal.

import src.genetic_algo as ga
import src.simu_bike as sb
import src.scorePlot as sp

gen=ga.Genetic_algorithm(population_size=100,
                         start_point=(2,5),
                         side_length=3,
                         fit_func=sb.creat_bike)
x,y=gen.run()
sp.plot(x,y)