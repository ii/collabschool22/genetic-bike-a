.. Genetic Bike A documentation master file, created by
   sphinx-quickstart on Thu Jun 30 18:26:16 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Genetic Bike A's documentation!
==========================================




.. toctree::
   :maxdepth: 2
   :caption: Contents:

   genetic_algorithm
   gen



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
