Documentation for genetic algorithm module
+++++++++++++++++++++++++++++++++++++++++++



.. currentmodule:: genetic_algo


.. autofunction:: generate_random_coordinates

.. autofunction:: two_coordinates_are_the_same

.. autofunction:: one_coordinate


.. autoclass:: genetic_algo.Individual
   :members:


.. autoclass:: genetic_algo.Population
   :members:


.. autoclass:: Genetic_algorithm
   :members:


