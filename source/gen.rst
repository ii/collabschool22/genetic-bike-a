Documentation for gen file
++++++++++++++++++++++++++




.. currentmodule:: gen


 .. autofunction:: ground


 .. autofunction:: ground_force


 .. autofunction:: calc_spring_force

 .. autofunction:: calc_point_acceleration
