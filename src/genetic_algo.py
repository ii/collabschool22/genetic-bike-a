import random
import math
import json
import numpy as np


def two_coordinates_are_the_same(coordinates):
    """Checking if there are two coordinates in the given list of coordinates that are identical
    
    This function checks if there are two coordinates in the given list of coordinates that are identical.

    Parameters
    ----------
    coordinates : list
                 A list of four coordinates, each is [(x1, y1), (x2, y2)]

    returns: 
    ----------
    Bool
    Returns True if there are two identical ones, othereise, False
    """
    for i in range(len(coordinates)-1):
        for j in range(i+1, len(coordinates)):
            if coordinates[i] == coordinates[j]:
                return True
    return False

def one_coordinate(start_point, side_length):
    """Generate a coordinate
    
    This function generates a coordinate.

    Parameters
    ----------
    start_point : tuple
                 Two dimentional point representing a staring point for a square box
    side_lenght : int
                    A integer number representing the side lenght of a square pox


    returns: 
    ----------
    list of tuples
    Random coordinate of one point that is inside a square-box that is defined by the input parameters    
    """
    return [random.uniform(start_point[0], start_point[0]+side_length), random.uniform(start_point[1], start_point[1]+side_length)]

def generate_random_coordinates(start_point, side_length):
    """Generate a random coordinates!

    This function generates a random coordinates for four point inside a square box defined by a start_point point and length of the side

    Parameters
    ----------
    start_point : tuple
                 Two dimentional point representing a staring point for a square box
    side_lenght : int
                    A integer number representing the side lenght of a square pox


    returns: 
    ----------
    list of tuples
    Random coordinates of four points that are inside a square-box that is defined by the input parameters

    """
    coordinate = [one_coordinate(start_point, side_length),one_coordinate(start_point, side_length),one_coordinate(start_point, side_length),one_coordinate(start_point, side_length)]
    while two_coordinates_are_the_same(coordinate):
        coordinate = [one_coordinate(start_point, side_length),one_coordinate(start_point, side_length),one_coordinate(start_point, side_length),one_coordinate(start_point, side_length)]
    return coordinate



class Individual():
    """Indivual of four points

    ...
    Attributes
    ----------
    coordinates: list of tuples
                A list contanining four  2D points

    Methods
    -------

    set_coordinates(coordinates)
        reset Indivual by given coordinatge
    set_fitness_score(fitness_score)
        reset fitness score of individual 
    """
    def __init__(self, coordinates):
        self.coordinates = coordinates
        self.fitness_score = 0
    def set_coordinates(self, coordinates):
        self.coordinates = coordinates
    def set_fitness_score(self, fitness_score):
        self.fitness_score = fitness_score

class Population():
    """ A collection of Individual objects!


    ...
    Attributes
    ------------
    __size: int
            private integer that spesify the number of Individual object in a population
    __start_point : tuples
            private tuple  variable that spesify start point of square-box needed to create individual objects
    __side_length: int
            Private int number that spesify the side length of square-box needed to creeate individual objects 
    fit_func : function object
            A function object used to calculate fitness score of individual objects in the population
    population: list of all individual objects

    Methods
    -------
    get_size()
        get population size
    set_population(population)
        reset the population list to a given population list
    average_fitness_score():
        calculate average fitness score of population
    save_population_to_file(filename):
        save the population data into a json file
    """
    def __init__(self, size, start_point, side_length, fit_func):
        self.__size = size # this one should be a constant
        self.__start_point = start_point
        self.__side_length = side_length
        self.fit_func = fit_func
        self.population = [Individual(coordinates=generate_random_coordinates(self.__start_point, self.__side_length)) for _ in range(self.__size)]
        for i in self.population:
            i.set_fitness_score(self.fit_func(np.array(i.coordinates)))
    def get_size(self):
        return self.__size
    def set_population(self, population):
        self.population = population
    def average_fitness_score(self):
        total = 0
        for i in self.population:
            total += i.fitness_score    
        return total / self.__size
    def best_fitness_score(self):
        return max([x.fitness_score for x in self.population])
   
    def save_population_to_file(self,filename):
        with open(filename,"w") as f:
            json.dump([ind.coordinates for ind in self.population],f)
        f.close()

class Genetic_algorithm():
    '''Genetic Algorithm 

    

    A genetic algorithm (GA) is a form of evolutionary computation 
    that is especially suitable for optimization problems. 
    Following the process of natural evolution, 
    a problem is usually solved by following these main steps:

    START
    Generate the initial population
    Compute fitness
    REPEAT
        Selection
        Crossover
        Mutation
        Compute fitness
    UNTIL population has converged
    STOP

    Parameters
    ----------
    population_size : int
        the size of the population
    start : tuple
        the starting coordinate
    bound: int
        the side length of the area in which we generate the coordinates of the Individual objects
    fit_func: function name
        the function for calculating the fitness score
    '''

    def __init__(self, population_size, start_point, side_length, fit_func):
        self.__population_size = population_size # This also cannot be modified!
        self.__start_point = start_point
        self.__side_length = side_length
        self.fit_func = fit_func

    def selection(self, population, percentage=0.2):
        '''Select top individual



        Takes in a Population object and a percentage A%, returns a list of individuals 
        which are the top A% of the population that have the highest fitness score. 
        
        Parameters
        ----------
        population : object
            a Population object
        percentage : float
            the percentage of the population for the selection

        Returns
        -------
        list
            a list of Individual objects
        '''
        population_list = population.population
        sorted_population = sorted(population_list, key=lambda x: x.fitness_score)
        sorted_population.reverse()
        ######### shrink the size ########
        count_top_individuals = self.__population_size*percentage
        if count_top_individuals < 1:
            count_top_individuals = 1
        else:
            count_top_individuals = int(count_top_individuals)
        if count_top_individuals%2 != 0:
            count_top_individuals += 1
        self.population_size = count_top_individuals
        ##################################
        top_individuals = sorted_population[:count_top_individuals]
        return top_individuals
        
    def crossover(self, top_individuals):
        ''' Crossover population



        Use the genes of two parents and combine them to make up the child's genes. 

        This function takes in a list of individuals, and returns a list of new generated individuals. 
        Each generated individual is produced from two randomly selected individuals 
        (let's say A and B) from the input list. 
        Each generated one has 50% of the content from A and 50% of the content from B.
        The total amount of the generated children is the same as the original population size.

        Parameters
        ----------
        top_individuals : list
            a list of Individual objects

        Returns
        -------
        list
            a list of Individual objects
        '''

        children = []
        count_children = 0
        breaking_point = False
        while True:
            ### randomy select two and two as parents ###            
            random.shuffle(top_individuals)
            for i in range(0,len(top_individuals),2):
                mom = top_individuals[i]
                dad = top_individuals[i+1]
                #
                dad_low_mean = (dad.coordinates[0][1] + dad.coordinates[1][1]) / 2 
                mom_low_mean = (mom.coordinates[0][1] + mom.coordinates[1][1]) / 2 
                mom_dad_diff = abs((dad_low_mean + mom_low_mean) / 2) 
                # version 1
                #coordinate1 = [dad.coordinates[0], dad.coordinates[1], mom.coordinates[2], mom.coordinates[3]]
                # version 2
                coordinate1 = [[mom.coordinates[0][0], mom_dad_diff], [mom.coordinates[1][0], mom_dad_diff], mom.coordinates[2], mom.coordinates[3]]
                child1 = Individual(coordinates=coordinate1) 
                child1.set_fitness_score(self.fit_func(np.array(coordinate1)))
                children.append(child1)
                count_children += 1
                if count_children == self.__population_size:
                    breaking_point = True
                    break
                # version 1             
                #coordinate2 = [mom.coordinates[0], mom.coordinates[1], dad.coordinates[2], dad.coordinates[3]]
                # version 2
                coordinate2 = [[dad.coordinates[0][0], mom_dad_diff], [dad.coordinates[1][0], mom_dad_diff], dad.coordinates[2], dad.coordinates[3]]
                child2 = Individual(coordinates=coordinate2) 
                child2.set_fitness_score(self.fit_func(np.array(coordinate2)))
                children.append(child2)
                count_children += 1
                if count_children == self.__population_size:
                    breaking_point = True
                    break
            if breaking_point:
                break
        return children


    def mutation(self, population,mutation_frequency=0.2):
        '''shuffle coordinates of individual in a given population



        This function takes in a Population object, and modifies the coordinates of 
        each individual of the given population.

        Parameters
        ----------
        top_individuals : object
            a Population object
random.randint(start_point[1], start_point[1]+side_length)), 

        Returns
        -------
        None      
        '''
        random.shuffle(population.population) #
        children = population.population[:int(self.__population_size*mutation_frequency)]
        for c in children:
            gene_index=random.choice([0,1,2,3])
            c.coordinates[gene_index] = one_coordinate(self.__start_point, self.__side_length)
            c.set_fitness_score(self.fit_func(np.array(c.coordinates)))


    def run(self):
        '''Execute the genetic algorithm.

        Return a population, where the difference between the best fitness score and 
        the average fitness score is less than 0.5.
        '''
        # START   
        # Generate the initial population and Compute fitness     
        self.p = Population(self.__population_size, self.__start_point, self.__side_length, self.fit_func)  
        average_fitness_score = self.p.average_fitness_score()
        # REPEAT  
        last_ten_average = [0]*10
        file_counter=0
        score_list=[]
        high_score_list=[]
        while abs(average_fitness_score- np.mean(last_ten_average[-10:])) > 0.1:
            last_ten_average.append(self.p.average_fitness_score())
            self.p.save_population_to_file("data/population_"+str(file_counter)+".json")
            file_counter+=1
            # Selection
            top_individuals = self.selection(self.p, percentage=0.2)    
            # Crossover
            children_of_top_individuals = self.crossover(top_individuals)
            self.p.set_population(children_of_top_individuals)
            # Mutation and Compute fitness        
            self.mutation(self.p)
            average_fitness_score = self.p.average_fitness_score()
            last_10_average = sum(last_ten_average[-10:]) / len(last_ten_average[-10:])
            print(f'{average_fitness_score=}, {last_10_average=}, {len(self.p.population)}')                     
            # UNTIL population has converged
            # STOP
            score_list.append(average_fitness_score)
            high_score_list.append(self.p.best_fitness_score())
            if len(score_list)>100:
                break
        return score_list, high_score_list
        #return the fittest child among the last siblings
        # remaining_sibling = sorted(p.population, key=lambda x: x.fitness_score)
        # remaining_sibling.reverse()         
        # return remaining_sibling[0]
        

            

if __name__ == '__main__':
    algo = Genetic_algorithm(population_size = 10,
                                start_point = (0,0),
                                side_length = 5,
                                fit_func= calculate_distance)


    algo.run()
    print(f'{[i.coordinates for i in algo.p.population]=}')
    
    # print(algo.__doc__)
    # print(algo.selection.__doc__)
    # print(algo.crossover.__doc__)
    # print(algo.mutation.__doc__)
    # print(algo.run.__doc__)

