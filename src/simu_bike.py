import src.gen as g
import numpy as np

# for test
def creat_bike(points_position):
    """creat a bike and run the bike simulation, then return the distance.

    Args:
        points_position (np.array): a 4x2 np.array
    """
    num_point=len(points_position)
    start_x = np.max(points_position[:, 0])
    points_velocity = np.zeros_like(points_position)

    points_mass = np.array([20, 20, 0.1, 0.1])  # unit [m]
    wheel_radius = 1.2  # unit [m]

    spring_indexs = []
    for i in range(num_point):
        for j in range(num_point):
            if i != j:
                spring_indexs.append([i, j])
    spring_indexs = np.array(spring_indexs)

    spring_stiffness = 1e3  # unit [N/m] a.k.a -> K

    gravity = -9.81  # unit [ m /s2]
    spring_initial_length = g.calc_spring_length(points_position, spring_indexs)

    def update(points_position,points_velocity):
        # boundary condition 1
        if np.min(points_position[2:, 1] - g.ground(points_position[2:, 0])) < 0:
            return(np.max(points_position[:, 0])-start_x)
        else:
            state_y = points_position[:, 1] - g.ground(points_position[:, 0])
            state_y[0] -= wheel_radius
            state_y[1] -= wheel_radius
            # update position
            points_position += points_velocity*dt
            # update velocity
            spring_length = g.calc_spring_length(points_position, spring_indexs)
            spring_force = g.calc_spring_force(
                spring_length-spring_initial_length, spring_stiffness)
            points_velocity += g.calc_point_acceleration(
                points_position, spring_indexs, spring_force, points_mass)*dt
            points_velocity[:,
                            1] += g.ground_force(state_y, spring_stiffness, points_mass)*dt
            points_velocity[:, 1] += gravity*dt
            # damp the vertical velocity to make the solution stable
            yfliter = state_y * points_velocity[:, 1] > 0
            points_velocity[:, 1][yfliter] *= 0.01
            # boundary condition 2
            if points_position[0, 1] < wheel_radius+g.ground(points_position[0, 0]):
                points_velocity[0, 0] = 2

    # for time-domain solver
    run_time = 15  # unit [s]
    dt = 0.01     # unit [s]
    
    for i in np.arange(1, int(run_time/dt)):
        update(points_position,points_velocity)
    return (np.max(points_position[:, 0])-start_x)

