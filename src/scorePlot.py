import matplotlib.pyplot as plt

def plot(scores,high_score):
    plt.plot(range( len(scores)),scores,'--b')
    plt.plot(range( len(high_score)),high_score,'-r')
    plt.xlabel('Generation')
    plt.ylabel('Ave. score')
    plt.show()
