import matplotlib.pyplot as plt

def plot(scores):
    plt.plot(range( len(scores)),scores,'--b')
    plt.show()
