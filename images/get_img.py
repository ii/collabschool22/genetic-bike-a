import numpy as np
import matplotlib.pyplot as plt
import json
import os
def save_bike_image(points_position, counter):
    """This function save an image of the bike.

    :param points_position: provide bike coordinates as numpy array
    :param counter: provide bike number
    :return: bike image file
    """
    fig, ax = plt.subplots()
    ax.set_title(f"Bike {counter}")
    ax.axis('equal')
    ax.set_xlim(0, 15)
    ax.set_ylim(-5, 10)

    num_point=4
    wheel_radius = 1.2
    spring_indexs = []
    for i in range(num_point):
        for j in range(num_point):
            if i!=j:
                spring_indexs.append([i,j])
    spring_indexs=np.array(spring_indexs)
    ax.plot(np.arange(-10, 40, 0.5), np.sin(np.arange(-10, 40, 0.5)), 'k')  # ground

    balls = []
    balls.append(plt.Circle(
        (points_position[0][0], points_position[0][1]), wheel_radius, color='r'))
    balls.append(plt.Circle(
        (points_position[1][0], points_position[1][1]), wheel_radius))

    lines = []
    for item in spring_indexs:
        lines.append(ax.plot([points_position[item[0], 0], points_position[item[1], 0]],
                             [points_position[item[0], 1], points_position[item[1], 1]],
                             'o-', markersize=9, markerfacecolor='b'))
    for item in balls:
        ax.add_patch(item)
    plt.savefig(f"img_{counter}.png")

#num_point=4
#points_position = np.random.rand(num_point, 2)*8+3
#save_bike_image(points_position, 2)

list_files = os.listdir('../data')
for index,item in enumerate(list_files):
    os.makedirs(f'population_{index}', exist_ok=True)
    os.chdir(f'population_{index}')
    with open(f'../../data/{item}', 'r') as f:
        bikes = json.load(f)
        print(len(bikes))
        f.close()
        for bike in range(len(bikes)):
            print(bike)
            save_bike_image(np.array(bikes[bike]),bike)
    os.chdir('../')
#print(type(bikes[0][0]))