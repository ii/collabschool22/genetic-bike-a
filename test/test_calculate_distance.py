import pytest

import src.utils


# def test_calculate_distance_exists():
#     from ../src/utils import calculate_distance

tuples_of_2d_points_and_their_distance = [
    ((0, 0), (5, 0), 5),
    ((0, 0), (0, 7), 7),
    ((-7, -4), (17, 6.5), 26.196373794859472),
    ((1, 1), (4, 5), 5),
    ((100, 43), (6, 5), 101.39033484509261),
]


@pytest.mark.parametrize("p1,p2,expected", tuples_of_2d_points_and_their_distance)
def test_calculate_distance(p1, p2, expected):
    assert src.utils.calculate_distance(p1, p2) == expected
