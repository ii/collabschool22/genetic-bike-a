"""
An example from David Grellscheid
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


# dots = np.array([
#     [0.3, 0.8, 0.02, 0.03],
#     [0.2, 0.1, 0.05, -0.01],
# ])

dots = np.random.rand(1000,4)

xs = dots[:,0]   # start:end   :step
ys = dots[:,1]

vxs = dots[:,2]
vys = dots[:,3]

dots[:,2:] -= 0.5
dots[:,2:] *= 0.04


fig, ax = plt.subplots()

plot_dots, = plt.plot(xs, ys, 'ob')

def update(frame):
    global xs, ys, vxs, vys
    xs += vxs # time = 1
    ys += vys 

    #vys -= 0.001

    xfilter = (xs > 1) | (xs < 0) 
    vxs[xfilter] *= -0.95
    
    yfilter = (ys > 1) | (ys < 0) 
    vys[yfilter] *= -0.95

    print('Energy',np.sum(vxs**2 + vys**2))

    plot_dots.set_data(xs, ys)
    return plot_dots,


plt.xlim(0,1)
plt.ylim(0,1)

ani = FuncAnimation(
    fig,
    update,
    #frames=40,
    interval=50,
    repeat=False,
)

plt.show()
