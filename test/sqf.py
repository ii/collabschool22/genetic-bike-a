
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import squareform as sqf

force2=np.array([[1,2],
                 [3,4],
                 [5,6],
                 [7,8],
                 [9,10],
                 [11,12]])
force1=np.array([1,2,3,4,5,6])
big_ma2=sqf(force1)*np.tri(4)*(-2)+sqf(force1)

print(big_ma2)


force2_x=force2[:,0]
big_ma2=sqf(force2_x)*np.tri(4)*(-2)+sqf(force2_x)
print(big_ma2)
